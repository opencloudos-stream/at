Summary:		Time-oriented job management utilities
Name:			at
Version:		3.2.5
Release:		6%{?dist}
License:		GPLv3+ and GPLv2+ and ISC and MIT and Public Domain
URL:			http://blog.calhariz.com/index.php/tag/at
Source:			http://software.calhariz.com/at/%{name}_%{version}.orig.tar.gz
Source1:		pam_atd
Source2:		atd.sysconf
Source3:		atd.systemd
# add upstream files config.guess and config.sub
# repository: https://git.savannah.gnu.org/git/config.git
# commit id: 02ba26b218d3d3db6c56e014655faf463cefa983
Source4:		config.guess
Source5:		config.sub

Patch3000:		at-3.2.5-make.patch
Patch3001:      at-3.2.2-shell.patch
Patch3002:      at-3.1.14-fix_no_export.patch
Patch3003:      at-3.1.16-clear-nonjobs.patch
Patch3004:      at-3.2.2-lock-locks.patch
Patch3005:      at-3.1.20-log-jobs.patch

BuildRequires:	gcc flex flex-static bison autoconf make automake
BuildRequires:	perl(Test::Harness) perl(Test::More) pam-devel smtpdaemon
BuildRequires:	libselinux-devel >= 1.27.9
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units

%description
This package provides time-oriented job management utilities. at and
batch read commands from standard input or a specified file which are
to be executed at a later time. atd runs jobs queued by at. atq lists
the user's pending jobs. atrm deletes jobs, identified by their job 
number.

%prep
%autosetup -p1
cp %{SOURCE1} %{SOURCE4} %{SOURCE5} .

%build
autoreconf -fiv
%configure --with-atspool=%{_localstatedir}/spool/at/spool \
    --with-jobdir=%{_localstatedir}/spool/at \
    --with-daemon_username=root  \
    --with-daemon_groupname=root \
    --with-selinux \
	--with-pam
%make_build

%install
%make_install \
    prefix=%{_prefix} \
    exec_prefix=%{_prefix} \
    etcdir=%{_sysconfdir} \
    datadir=%{_datadir} \
    bindir=%{_bindir} \
    sbindir=%{_sbindir} \
    docdir=%{_defaultdocdir} \
    mandir=%{_mandir} \
    ATJOB_DIR=%{_localstatedir}/spool/at \
    ATSPOOL_DIR=%{_localstatedir}/spool/at/spool \
    DAEMON_USERNAME=`id -nu` \
    DAEMON_GROUPNAME=`id -ng` \
    INSTALL_ROOT_USER=`id -nu` \
    INSTALL_ROOT_GROUP=`id -nu`

install -D -m 0644 %{SOURCE2} %{buildroot}/etc/sysconfig/atd
install -D -m 0644 %{SOURCE3} %{buildroot}%{_unitdir}/atd.service

echo > %{buildroot}%{_sysconfdir}/at.deny

rm -f %{buildroot}%{_defaultdocdir}/%{name}/Copyright
rm -f %{buildroot}%{_defaultdocdir}/%{name}/Problems

%check
make test

%post
touch %{_localstatedir}/spool/at/.SEQ
chmod 600 %{_localstatedir}/spool/at/.SEQ
chown root:root %{_localstatedir}/spool/at/.SEQ
%systemd_post atd.service

%preun
%systemd_preun atd.service

%postun
%systemd_postun_with_restart atd.service

%files
%license Copyright COPYING
%doc README timespec ChangeLog
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/at.deny
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/sysconfig/atd
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/pam.d/atd
%attr(0700,root,root) %dir %{_localstatedir}/spool/at
%attr(0700,root,root) %dir %{_localstatedir}/spool/at/spool
%attr(0600,root,root) %verify(not md5 size mtime) %ghost %{_localstatedir}/spool/at/.SEQ
%attr(0644,root,root) %{_unitdir}/atd.service
%{_datadir}/at/batch-job
%attr(0755,root,root) %{_sbindir}/atd
%{_sbindir}/atrun
%attr(4755,root,root) %{_bindir}/at
%{_bindir}/atq
%{_bindir}/atrm
%{_bindir}/batch
%{_mandir}/man*/*

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.2.5-6
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.2.5-5
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.2.5-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.2.5-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.2.5-2
- Rebuilt for OpenCloudOS Stream 23

* Mon Jul 25 2022 Xiaojie Chen <jackxjchen@tencent.com> - 3.2.5-1
- Initial build
